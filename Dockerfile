FROM php:7.4

# Update packages
RUN apt-get update

# Install PHP and composer dependencies
RUN apt-get install -qqq git curl libmcrypt-dev libjpeg-dev libpng-dev \
    libfreetype6-dev libbz2-dev libzip-dev

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
# https://stackoverflow.com/questions/47671108/docker-php-ext-install-mcrypt-missing-folder
RUN pecl install mcrypt-1.0.3
RUN docker-php-ext-enable mcrypt
RUN docker-php-ext-install pdo_mysql zip

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | \
    php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel Envoy
RUN composer global require "laravel/envoy=~1.0"

# Clear out the local repository of retrieved package files
RUN apt-get clean
RUN docker-php-source delete